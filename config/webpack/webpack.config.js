const path = require('path');
// Global paths
const buildPath = path.resolve(process.cwd(), 'build');
const configPath = path.resolve(process.cwd(), 'config');
const sourcePath = path.resolve(process.cwd(), 'src');
const publicPath = 'http://localhost:8888/';

// Loaders and plugins
const autoprefixer = require('autoprefixer');
const ExtractCSS = require('extract-text-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const StyleLint = require('stylelint-webpack-plugin');
const webpack = require('webpack');

// Local configs
const appVersion = require('../../package.json').version;
const appConfig = require('../env/dev.json');


// Webpack configs ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
module.exports = {
  context: sourcePath,
  devtool: 'eval',

  entry: {
    app: ['babel-polyfill', 'index.jsx'],
    vendor: [
      'prop-types',
      'react',
      'react-dom',
      'react-intl',
      'react-redux',
      'redux',
      'redux-thunk'
    ]
  },

  output: {
    filename: 'static/js/[chunkhash:8].[name].js',
    path: buildPath,
    publicPath
  },

  resolve: {
    extensions: ['.js', '.jsx', '.scss'],
    alias: {
      components: `${sourcePath}/components`,
      elements: `${sourcePath}/elements`,
      styles: `${sourcePath}/assets/styles`,
      scripts: `${sourcePath}/assets/scripts`,
      images: `${sourcePath}/assets/images`
    },
    modules: [
      'node_modules',
      sourcePath
    ]
  },


  // dev-server configs :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  devServer: {
    contentBase: sourcePath,
    compress: true,
    port: 8888,
    // open: true,
    stats: {
      assets: true,
      children: false,
      modules: false
    },
    // Services config
    headers: {
      'X-WM-Token': 'yes'
    },
    proxy: {
      '/services/*': {
        target: 'https://qa.checkout.waldev.com.br/checkout',
        changeOrigin: true
        // secure: false
      }
    }
  },


  // Modules ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  module: {
    rules: [
      { // eslint config
        test: /\.jsx?$/,
        enforce: 'pre',
        include: sourcePath,
        loader: 'eslint-loader',
        options: {
          configFile: `${configPath}/.eslintrc`
          // fix: true // autofix eslint errors
        }
      },

      { // babel config
        test: /\.jsx?$/,
        include: sourcePath,
        use: [
          'babel-loader'
        ]
      },

      { // scss config
        test: /\.scss$/,
        include: sourcePath,
        use: ExtractCSS.extract({
          fallback: 'style-loader',
          use: [
            'css-loader',
            {
              loader: 'postcss-loader',
              options: {
                plugins: [autoprefixer]
              }
            },
            'sass-loader'
          ]
        })
      },

      {
        test: /\.(png|jpe?g|gif|svg)$/,
        include: `${sourcePath}/assets/images`,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: 'static/img/[name].[ext]',
              limit: 12000
            }
          }
        ]
      }

    ]
  },


  // Plugins ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  plugins: [
    // Extract CSS
    new ExtractCSS('static/css/[contenthash:8].[name].css', { allChunks: true }),

    // Generate HTML file
    new HtmlPlugin({
      template: 'index.html',
      minify: {
        collapseWhitespace: true,
        removeComments: true,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true
      }
    }),

    // Lint scss files
    new StyleLint({
      configFile: `${configPath}/.stylelintrc/`,
      context: sourcePath,
      syntax: 'scss'
    }),

    // Add global object config
    new webpack.DefinePlugin({
      app: {
        config: JSON.stringify(appConfig),
        env: JSON.stringify('dev'),
        version: JSON.stringify(appVersion)
      }
    }),

    // Build separeted vendor file
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity
    }),

    // Minify CSS
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })

    // Minify JS
    // new webpack.optimize.UglifyJsPlugin({
    //   sourceMap: false,
    //   compress: true
    // }),
    // Minify scripts
    // new webpack.optimize.UglifyJsPlugin()

  ]
};
