// setup file
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, render, mount, configure } from 'enzyme';
import devConfigs from './env/dev.json';


// Make React and Enzyme functions available in all test files without importing
global.React = React;
global.shallow = shallow;
global.render = render;
global.mount = mount;


// Webpack global configs (DefinePlugin)
global.app = {
  config: devConfigs
};

configure({ adapter: new Adapter() });


// Skip createElement warnings but fail tests on any other warning
// console.error = message => {
//   if (!/(React.createElement: type should not be null)/.test(message)) {
//     throw new Error(message);
//   }
// };
