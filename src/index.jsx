import React from 'react';
import { render } from 'react-dom';

// Redux
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

// Intl
import { IntlProvider, addLocaleData } from 'react-intl';
import pt from 'react-intl/locale-data/pt';
import messages from './assets/locales/pt-br.json';

// App
import reducers from './redux';
import Cart from './components/cart';

addLocaleData([...pt]);

const store = createStore(reducers, applyMiddleware(thunk));
const cartApp = (
  <Provider store={store}>
    <IntlProvider locale="pt" messages={messages}>
      <Cart />
    </IntlProvider>
  </Provider>
);

render(cartApp, document.querySelector('#cart-app'));
