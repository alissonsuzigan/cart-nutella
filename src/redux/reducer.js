import { LOADING, LOAD_CART, LOAD_CART_ERROR } from './actions';
import { getItems, getResume } from './selectors';

// Default configs ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const DEFAULT_STATE = {
  error: false,
  loading: false
};

const DEFAULT_ACTION = {
  type: LOADING
};


// Reducer ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
export default function reducer(state = DEFAULT_STATE, action = DEFAULT_ACTION) {
  let cartData;

  switch (action.type) {
    case LOADING:
      return {
        ...state,
        ...DEFAULT_STATE,
        loading: true
      };

    case LOAD_CART:
      cartData = action.payload;
      return {
        ...state,
        ...DEFAULT_STATE,
        items: getItems(cartData),
        resume: getResume(cartData)
      };

    case LOAD_CART_ERROR:
      cartData = action.payload.errorMap && action.payload.errorMap.cart ? action.payload.errorMap.cart : {};
      return {
        ...state,
        ...DEFAULT_STATE,
        error: true,
        items: getItems(cartData),
        resume: getResume(cartData)
      };

    default:
      return state;
  }
}
