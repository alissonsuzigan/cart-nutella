// Component selectors ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
export const getTotalItems = items => (
  items && items.reduce((value, item) => value + item.quantity, 0)
);


// Reducer selectors ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
export const getItems = (payload) => {
  const { items } = payload;
  return items || [];
};

export const getResume = payload => ({
  subtotal: payload.subtotal || null,
  totalPrice: payload.totalPrice || null,
  bestPrice: payload.totalPriceWithEstimatedBestShipping || null
});

// export const getData = (payload) => {
//   const { items, ...data } = payload;
//   return data;
// };
