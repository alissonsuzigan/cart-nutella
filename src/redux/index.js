import { combineReducers } from 'redux';
import cart from './reducer';

const reducers = combineReducers({
  cart
});

export default reducers;
