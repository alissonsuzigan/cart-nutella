// Action Types :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
export const LOADING = 'LOADING';
export const LOAD_CART = 'LOAD_CART';
export const LOAD_CART_ERROR = 'LOAD_CART_ERROR';

const cookies = { credentials: 'same-origin' };


// Action Creators ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Load Cart
export const loadCart = () => {
  const url = `${app.config.domain}${app.config.services.loadCart.url}`;

  return (dispatch) => {
    dispatch({ type: LOADING });

    fetch(url, cookies)
      .then(response => (response.ok ? response.json() : response.json().then(Promise.reject.bind(Promise))))
      .then(result => dispatch({
        type: LOAD_CART,
        payload: result
      }))
      .catch(error => dispatch({
        type: LOAD_CART_ERROR,
        payload: error
      }));
  };
};
