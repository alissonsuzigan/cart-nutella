import React from 'react';

import Translation from './translation';


describe('<Translation />', () => {
  const propValues = {name: "checkout"};
  const component = shallow(<Translation textKey="textKey" values={propValues} />);

  test('should render a translated element with defined values and defined textKey', () => {
    expect(component.find('FormattedMessage').prop('id')).toEqual('textKey');
    expect(component.find('FormattedMessage').prop('values')).toEqual({name: "checkout"});
  });
});
