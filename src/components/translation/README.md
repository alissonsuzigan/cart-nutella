# Componente de tradução **[stateless]**
> Componente para traduções baseado no **react-intl**

Componente para utilização de traduções na aplicação. Baseado no **react-intl** e utilizando seu componente **FormatedMessage** para inserção de elementos traduzidos a partir de prévia configuração.

O componente trabalha utilizando como *asset* um arquivo de tradução (localizado em *src/assets/locales*) e espera algumas *props* como parâmetro para sua execução correta.


## Como utilizar

Importar o componente **Translation** através de sua pasta (*src/components/translation*) ou através de seu arquivo .jsx, localizado no mesmo diretório.

## Tabela de *props*

### Props necessárias
Valores esperados e suas utilizações no componente

|   Prop   |   Utilização                                |   Exemplo        |
|:--------:|:-------------------------------------------:|:----------------:|
|key       | reconhecimento da chave de tradução no json | cart.totalItems  |


### Props opcionais
Valores opcionais, suas utilizações no componente e seus valores *default*

|   Prop    |   Utilização                                  |   Exemplo                    | Default |
|:---------:|:---------------------------------------------:|:----------------------------:|:-------:|
|values     | inserção de variáveis dentro da tradução      | { pageName: "Seu carrinho" } | {}      |
|tagName    | definir a tag que envolverá o texto traduzido | h1                           | span    |

**Qualquer outra prop passada ao instanciar o componente (assim como className) será inserida na tag renderizada**
