import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';


// Component :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function Translation({ textKey, values, tagName, ...props }) {
  const Tagname = tagName;
  return (
    <FormattedMessage id={textKey} values={values}>
      { text => <Tagname {...props}>{text}</Tagname> }
    </FormattedMessage>
  );
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Translation.propTypes = {
  textKey: PropTypes.string.isRequired,
  tagName: PropTypes.string,
  values: PropTypes.shape({})
};

Translation.defaultProps = {
  tagName: 'span',
  values: {}
};

export default Translation;
