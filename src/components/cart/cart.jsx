import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

// Components
import Header from 'components/header';
import Footer from 'components/footer';
import CartEmpty from 'components/cart-empty';
import CartShop from 'components/cart-shop';

// Redux
import { connect } from 'react-redux';
import { loadCart } from '../../redux/actions';
import { getTotalItems } from '../../redux/selectors';


class Cart extends PureComponent {
  componentDidMount() {
    this.props.loadCart();
  }

  renderCart() {
    const { items } = this.props.cart;

    if (items && items.length > 0) {
      return <CartShop data={this.props.cart} />;
    }
    return <CartEmpty />;
  }

  render() {
    const { cart } = this.props;

    return (
      <section id="wm-cart" className="wm-cart">
        {app.env === 'dev' && <Header />}
        <main className="cart-main">
          {cart && cart.items && this.renderCart()}
        </main>
        <Footer />
      </section>
    );
  }
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Cart.propTypes = {
  cart: PropTypes.shape({
    items: PropTypes.arrayOf(PropTypes.object)
  }).isRequired,
  loadCart: PropTypes.func.isRequired
};


// Redux ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const mapStateToProps = ({ cart }) => ({
  cart: {
    ...cart,
    totalItems: getTotalItems(cart.items)
  }
});

export default connect(mapStateToProps, { loadCart })(Cart);
