import Header from './header';


describe('Test <Header />', () => {
  const wrapper = shallow(<Header />);

  test('should verify header structure', () => {
    expect(wrapper.find('header#cart-header.cart-header')).toHaveLength(1);
    expect(wrapper.find('div.wrapper')).toHaveLength(1);
    expect(wrapper.children()).toHaveLength(1);
  });

  test('should verify header-title structure', () => {
    expect(wrapper.find('h1.cart-header-title')).toHaveLength(1);
    expect(wrapper.find('h1.cart-header-title').text()).toBe('Walmart.com');
  });

});