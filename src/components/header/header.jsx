import React from 'react';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function Header() {
  return (
    <header id="cart-header" className="cart-header">
      <div className="wrapper">
        <h1 className="cart-header-title">Walmart.com</h1>
      </div>
    </header>
  );
}

export default Header;
