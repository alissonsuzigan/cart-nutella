import CartEmpty from './cart-empty';


describe('<CartEmpty />', () => {
  const wrapper = shallow(<CartEmpty />);

  test('should verify CartEmpty structure', () => {
    expect(wrapper.find('section.cart-empty.wrapper')).toHaveLength(1);
    expect(wrapper.children()).toHaveLength(2);
    expect(wrapper.find('Translation')).toHaveLength(3);
  });

  test('should verify CartEmpty header element', () => {
    const header = wrapper.find('header.cart-empty-header');
    expect(header.children()).toHaveLength(2);
    // <h1>
    const h1 = header.childAt(0);
    expect(h1).toHaveLength(1);
    expect(h1.name()).toBe('Translation');
    expect(h1.prop('textKey')).toBe('empty.title');
    expect(h1.prop('tagName')).toBe('h1');
    expect(h1.prop('className')).toBe('cart-empty-title');
    // <h2>
    const h2 = header.childAt(1);
    expect(h2).toHaveLength(1);
    expect(h2.name()).toBe('Translation');
    expect(h2.prop('textKey')).toBe('empty.subtitle');
    expect(h2.prop('tagName')).toBe('h2');
    expect(h2.prop('className')).toBe('cart-empty-subtitle');
  });

  test('should verify CartEmpty footer element', () => {
    const footer = wrapper.find('footer.cart-empty-footer');
    expect(footer).toHaveLength(1);
    expect(footer.find('Button')).toHaveLength(1);
    expect(footer.find('Button').prop('href')).toEqual('https://qa.webstore.waldev.com.br');
    expect(footer.find('Translation').prop('textKey')).toEqual('empty.back-to-store');
  });
});
