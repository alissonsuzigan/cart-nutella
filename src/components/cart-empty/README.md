# Componente CartEmpty [stateless]
Usado para gerar a página de carrinho vazio.


## Como utilizar
```jsx
import CartEmpty from 'components/cart-empty';

const TestCartEmpty() {
  return <CartEmpty />;
}
```


## Propriedades
Este componente não possui propriedades.
