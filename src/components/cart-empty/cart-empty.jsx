import React from 'react';

// Components
import Translation from 'components/translation';

// Elements
import Button from 'elements/button';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function CartEmpty() {
  return (
    <section className="cart-empty wrapper">
      <header className="cart-empty-header">
        <Translation textKey="empty.title" tagName="h1" className="cart-empty-title" />
        <Translation textKey="empty.subtitle" tagName="h2" className="cart-empty-subtitle" />
      </header>

      <footer className="cart-empty-footer">
        <Button href={app.config.endpoints.store}>
          <Translation textKey="empty.back-to-store" />
        </Button>
      </footer>
    </section>
  );
}

export default CartEmpty;
