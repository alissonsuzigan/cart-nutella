import React from 'react';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function Footer() {
  return (
    <footer id="cart-footer" className="cart-footer">
      <div className="wrapper">
        <p className="footer-text">Walmart.com</p>
      </div>
    </footer>
  );
}

export default Footer;
