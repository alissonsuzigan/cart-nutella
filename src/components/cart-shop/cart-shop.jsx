import React from 'react';
import PropTypes from 'prop-types';

import Translation from '../translation';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function CartShop({ data }) {
  console.log(data);
  return (
    <section className="cart-shop wrapper">
      <Translation
        textKey="cart.totalItems"
        values={{ totalItems: data.totalItems }}
        tagName="h1"
      />
    </section>
  );
}

// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
CartShop.propTypes = {
  data: PropTypes.shape({
    totalItems: PropTypes.number.isRequired
  }).isRequired
};

export default CartShop;
