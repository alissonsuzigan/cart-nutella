import React from 'react';
import PropTypes from 'prop-types';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function Button({ children, className, skin, ...others }) {
  const btnClass = ['btn', skin.trim() ? `btn-${skin.trim()}` : 'btn-default', className.trim()].join(' ').trim();
  const Tag = others.href ? 'a' : 'button';


  // Handle events ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  const handleClick = (event) => {
    event.currentTarget.blur();
    if (others.disabled) event.preventDefault();
    if (others.onClick) others.onClick(event);
  };


  // Component props ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  const props = {
    ...others,
    className: btnClass,
    onClick: handleClick
  };

  return (
    <Tag {...props}>
      {children}
    </Tag>
  );
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Button.defaultProps = {
  className: '',
  disabled: false,
  href: null,
  onClick: null,
  skin: 'default'
};

Button.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node
  ]).isRequired,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  href: PropTypes.string,
  onClick: PropTypes.func,
  skin: PropTypes.oneOf([
    'default', 'default-border',
    'success', 'success-border',
    'warning', 'warning-border',
    'error', 'error-border',
    'link'
  ])
};

export default Button;
