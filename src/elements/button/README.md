# Componente Button [stateless]

Usado para criar **botões** `<button />` e **links** `<a href="/url" />` com aparência de botão.

Este componente possui 5 estados visuias `default | success | warning | error | link` e 2 modos de exibição para cada estado (exceto para `link`) `normal | border` que é definido na propriedade `skin`.


## Como utilizar
```jsx
import Button from 'elements/button';

const TestButtons() {
  return (
    <div>
      // Botão padrão | cor azul
      <Button>Botão padrão</Button>
      <Button skin="default">Botão padrão</Button>
      <Button skin="default-border">Botão padrão</Button>

      // Botão de sucesso | cor verde
      <Button skin="success">Botão de sucesso</Button>
      <Button skin="success-border">Botão de sucesso</Button>

      // Botão de alerta | cor amarelo
      <Button skin="warning">Botão de alerta</Button>
      <Button skin="warning-border">Botão de alerta</Button>

      // Botão de erro | cor vermelho
      <Button skin="error">Botão de erro</Button>
      <Button skin="error-border">Botão de erro</Button>

      // Botão desabilitado
      <Button disabled>Botão desabilitado</Button>
      <Button className="disabled">Botão desabilitado</Button>

      // Botão exbilido como link
      <Button skin="link">Botão exbilido como link</Button>

      // Botão renderizado como link
      <Button href="/url">Botão padrão</Button>
      <Button href="/url" skin="success">Botão padrão</Button>
      <Button href="/url" skin="default-border">Botão padrão</Button>
    </div>
  );
}
```

## Propriedades

| Prop      | Type    | Default   | Utilização                                                          |
|:---------:|:-------:|:---------:|:-------------------------------------------------------------------:|
| className | String  | `''`      | Adiona outras classes ao componente                                 |
| disabled  | Boolean | `false`   | Caso verdadeiro, o botão é desabilitado                             |
| href      | String  | `null`    | Define a url de destino e renderiza o componente como link `<a />`  |
| skin      | String  | `default` | Define o estado e o visual do botão                                 |


**Atenção:** A propriedade `skin` aceira somente os valores:
`default | default-border | success | success-border | warning | warning-border | error | error-border | link`.
Qualquer outro valor irá gerar um erro no componente.


**As demais propriedades passadas ao instanciar o componente (assim como className) serão inseridas na tag renderizada.**
