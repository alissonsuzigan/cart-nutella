import Button from './button';


describe ('<Button>', () => {
  test('should use default structure', () => {
    const button = shallow(<Button>Button label</Button>);
    expect(button.type()).toBe('button');
    expect(button.text()).toBe('Button label');
    expect(button.hasClass('btn-default')).toBe(true);
    expect(button.prop('className')).toBe('btn btn-default');
  });

  test('should use custom class and skin', () => {
    const button = shallow(<Button skin="success" className="checkout">Custon button</Button>);
    expect(button.hasClass('btn-success')).toBeTruthy();
    expect(button.hasClass('checkout')).toBeTruthy();
    expect(button.prop('className')).toBe('btn btn-success checkout');
  });

  test('should render as anchor element', () => {
    const button = shallow(<Button href="/test">Anchor button</Button>);
    expect(button.type()).toBe('a');
    expect(button.prop('href')).toBe('/test');
    expect(button.prop('className')).toBe('btn btn-default');
    expect(button.html()).toBe('<a href="/test" class="btn btn-default">Anchor button</a>');
  });
});
